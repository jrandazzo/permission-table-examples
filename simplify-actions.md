### Simplify Actions

Problem to solve: There are very granular actions listed out on the table, especially for "update-like" activities on the resource. This can make the table long and verbose. See [issues or merge requests](https://docs.gitlab.com/ee/user/permissions.html) as an example. The tables below demonstrate simplifying the actions to View/Create/Update/Delete with a footnote on actions related to `Update` .

#### Issues

| Action     | Guest | Reporter | Developer | Maintainer | Owner | Notes |
| ---------- | ----- | -------- | --------- | ---------- | ----- | ----- |
| View       | ✓     | ✓        | ✓         | ✓          | ✓     |       |
| Create     | ✓     | ✓        | ✓         | ✓          | ✓     |       |
| Update*    |       | ✓        | ✓         | ✓          | ✓     |       |
| Delete     |       |          |           |            | ✓     |       |

*Updating issues includes setting metadata such as labels, assign, weight, milestones, time tracking, designs, and setting parrent epic.

#### Merge Requests

| Action     | Guest | Reporter | Developer | Maintainer | Owner | Notes |
| ---------- | ----- | -------- | --------- | ---------- | ----- | ----- |
| View       | ✓     | ✓        | ✓         | ✓          | ✓     |       |
| Create     |       |          | ✓         | ✓          | ✓     |       |
| Update*    |       |          | ✓         | ✓          | ✓     |       |
| Delete     |       |          |           |            | ✓     |       |

*Updating merge requests include applying code suggestions, adding labels, locking threads, resolving threads, and assign

Use [^1] footnotes in place of `*`