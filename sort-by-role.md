### Sort by Role
This table demonstrates sorting by role where Guest permissions are at the top and Owner permissions are at the bottom. This should improve scanability of the actions.


| Action                        | Guest | Reporter | Developer | Maintainer | Owner |
| ----------------------------- | ----- | -------- | --------- | ---------- | ----- |
| View merge requests           |       | ✓        | ✓         | ✓          | ✓     |
| Create merge requests         |       |          | ✓         | ✓          | ✓     |
| Update merge requests         |       |          | ✓         | ✓          | ✓     |
| Manage merge request settings |       |          |           | ✓          | ✓     |
| Delete merge requests         |       |          |           |            | ✓     |