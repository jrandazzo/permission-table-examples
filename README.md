# Permission Table Examples

This projects contains various formats on how to organize [permissions table](https://docs.gitlab.com/ee/user/permissions.html) across the platform. 

- Example: [Sort by role](https://gitlab.com/jrandazzo/permission-table-examples/-/blob/main/sort-by-role.md)
- Example: [Group by category](https://gitlab.com/jrandazzo/permission-table-examples/-/blob/main/group-by-category.md)
- Example: [Simplify actions](https://gitlab.com/jrandazzo/permission-table-examples/-/blob/main/simplify-actions.md)

Inspiration
- https://gitlab.com/gitlab-org/gitlab/-/issues/467510
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/50241 
- Many other permission tables :)