### Group by category

#### Categories

This format demonstrates the permissions table broken down by categories. Instead of reading one long running table, it would be easier to see it broken down by sections. Below are potential categories along with a couple sample tables.

- Access (membership, custom roles, SAML, tokens)
- Analytics
- Application Security
- Compliance
- CI/CD: Artifacts, Environments, Jobs, Pipelines, Runners Variables
- Incident Management
- Groups and projects
- Project Planning: Boards, Iterations, Work Items, Wikis
- Registry
- Repository: branches, code, settings, merge requests
- Miscelananous: Billing


#### Open Questions
- How do handle group vs project permissions?
- Would a miscellaneous category be helpful to be a catch-all for those who may not be able to find a section?


Application Security

| Action                            | Guest | Reporter | Developer | Maintainer | Owner |
| --------------------------------- | ----- | -------- | --------- | ---------- | ----- |
| View licenses                     |       |          | ✓         | ✓          | ✓     |
| View dependencies                 |       |          | ✓         | ✓          | ✓     |
| View vulnerabilities              |       |          | ✓         | ✓          | ✓     |
| Manage security security policies |       |          | ✓         | ✓          | ✓     |
| Create issue from vulnerability   |       |          | ✓         | ✓          | ✓     |
| Change vulnerability status       |       |          |           | ✓          | ✓     |
| Assign security policy projects   |       |          |           |            | ✓     |

Compliance

| Action                       | Guest | Reporter | Developer | Maintainer | Owner |
| ---------------------------- | ----- | -------- | --------- | ---------- | ----- |
| View audit events            |       |          | ✓         | ✓          | ✓     |
| View compliance center       |       |          |           |            | ✓     |
| Manage compliance frameworks |       |          |           |            | ✓     |
| Manage audit event streaming |       |          |           |            | ✓     |
